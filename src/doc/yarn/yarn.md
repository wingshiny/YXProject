<!--
 * @Author: YinXuan
 * @Date: 2023-02-23 10:27:37
 * @LastEditTime: 2023-02-23 10:27:55
 * @Description:
-->

```
yarn / yarn install 一键安装jpackage.json所有包
yarn add [package] — 添加包，会自动安装最新版本，注意会覆盖指定版本号！！！
yarn add [package]@[version] — 带版本号安装
yarn remove [package] 移除某个包
yarn upgrade [package] 更新一个包
yarn add vue 安装一个包
yarn add vue@2.5.0  安装一个包
yarn upgrade vue 更新一个包
yarn upgrade vue@3.0.0 指定更新某个版本的包
yarn remove vue

yarn add vue 安装到dependencies
yarn add vue --save-dev 安装到devDependencies
```
