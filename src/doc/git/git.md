### git 常用指令

git clone
git status -s
git add .
git commit -m "吧啦吧啦"
git pull origin master
git push origin master
git checkout -b feature_xuan_v1.0
git branch
git checkout master
git merge origin feature_xuan_v1.0
git log

### git 提交代码规范

{value: 'feature', name: 'feature: 增加新功能'},
{value: 'bug', name: 'bug: 测试反馈 bug 列表中的 bug 号'},
{value: 'fix', name: 'fix: 修复 bug'},
{value: 'ui', name: 'ui: 更新 UI'},
{value: 'docs', name: 'docs: 文档变更'},
{value: 'style', name: 'style: 代码格式(不影响代码运行的变动)'},
{value: 'perf', name: 'perf: 性能优化'},
{value: 'refactor', name: 'refactor: 重构(既不是增加 feature，也不是修复 bug)'},
{value: 'release', name: 'release: 发布'},
{value: 'deploy', name: 'deploy: 部署'},
{value: 'test', name: 'test: 增加测试'},
{value: 'chore', name: 'chore: 构建过程或辅助工具的变动(更改配置文件)'},
{value: 'revert', name: 'revert: 回退'},
{value: 'build', name: 'build: 打包'}
