<!--
 * @Author: YinXuan
 * @Date: 2023-01-13 11:14:40
 * @LastEditTime: 2023-01-13 13:42:08
 * @Description: 生成头部注释的快捷键：ctrl + alt + i，生成函数注释的快捷键：ctrl + alt + t
-->

### Sass 的基础知识

##### 1. 定义变量

> _Sass 变量可以存储以下信息：字符串、数字、颜色值、布尔值、列表、null 值。_

```
// Sass 中定义变量使用 $ 符号。
$myFont: Helvetica, sans-serif;
$myColor: red;
$myFontSize: 18px;

p {
    color:$myColor;
    font-size:$myFontSize;
    font-family: $myFont;
}
```

##### 2. 导入样式

```
@import 'filename';
```

##### 3. 混入

> @mixin 指令允许我们定义一个可以在整个样式表中重复使用的样式。

> @include 指令可以将混入（mixin）引入到文档中。

```
@mixin important-text {
  color: red;
  font-size: 25px;
  font-weight: bold;
  border: 1px solid blue;
}
```

```
.danger {
  @include important-text;
  background-color: green;
}
```

生成的 CSS 代码

```
.danger {
  color: red;
  font-size: 25px;
  font-weight: bold;
  border: 1px solid blue;
  background-color: green;
}
```

> 混入之中可以传递变量（变量可以拥有默认值）

```
/*这段代码应该很好理解*/
// 混入接收两个参数
@mixin bordered($color, $width:1px) {
    border: $width solid $color;
}

.myArticle {
    // 调用混入，并传递两个参数
    @include bordered(blue, 1px);
}
```

> 混入可变参数 -- 有时，不能确定一个混入（mixin）或者一个函数（function）使用多少个参数，这时我们就可以使用 ... 来设置可变参数。

```
@mixin box-shadow($shadows...) {
    -moz-box-shadow: $shadows;
    -webkit-box-shadow: $shadows;
    box-shadow: $shadows;
}

.shadows {
    @include box-shadow(0px 4px 5px #666, 2px 6px 10px #999);
}
```

生成的 CSS 代码

```
.shadows {
    -moz-box-shadow: 0px 4px 5px #666, 2px 6px 10px #999;
    -webkit-box-shadow: 0px 4px 5px #666, 2px 6px 10px #999;
    box-shadow: 0px 4px 5px #666, 2px 6px 10px #999;
}
```

> 混入浏览器前缀 -- 我们也可以在混入中混入属性或者选择器。此时，变量必须用#{}包裹起来。

```
@mixin home($class,$direction){
    .#{$class}{
        margin-#{$direction}:5px;
    }
}
.test{
    @include home(div,left);
}
```
