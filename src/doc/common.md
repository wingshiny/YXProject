<!--
 * @Author: YinXuan
 * @Date: 2023-01-10 14:26:06
 * @LastEditTime: 2023-01-13 13:25:00
 * @Description: 生成头部注释的快捷键：ctrl + alt + i，生成函数注释的快捷键：ctrl + alt + t
-->

#### 预览 md 文件

`cmd-k v or ctrl-k v`

### 生成头部注释的快捷键：ctrl + alt + i，生成函数注释的快捷键：ctrl + alt + t
