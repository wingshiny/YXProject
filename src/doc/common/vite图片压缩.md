<!--
 * @Author: YinXuan
 * @Date: 2023-02-23 11:06:16
 * @LastEditTime: 2023-02-23 11:09:55
 * @Description: 参考链接：https://huaweicloud.csdn.net/638f1383dacf622b8df8ec62.html?spm=1001.2101.3001.6650.8&utm_medium=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~activity-8-124732352-blog-125424226.pc_relevant_3mothn_strategy_and_data_recovery&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~BlogCommendFromBaidu~activity-8-124732352-blog-125424226.pc_relevant_3mothn_strategy_and_data_recovery&utm_relevant_index=14
-->

### 安装 vite-plugin-imagemin

node vision: >= 12.0.0
vite vision: >= 2.0.0

> yarn add vite-plugin-imagemin -D

##### 因为 imagemin 在国内不好安装，所以提供解决方案

```
使用yarn在package.json内配置（推荐）
"resolutions": {
    "bin-wrapper": "npm:bin-wrapper-china"
}
```

##### 使用方法

```
import viteImagemin from 'vite-plugin-imagemin'
import { join, resolve } from 'path'
import vue from '@vitejs/plugin-vue'

export default ({ mode }: ConfigEnv): UserConfigExport => {
  return defineConfig({
    plugins: [
      vue(),
      viteCompression({
        ext: '.gz',
        algorithm: 'gzip',
        deleteOriginFile: false
      })
      viteImagemin({
        gifsicle: {
          optimizationLevel: 7,
          interlaced: false
        },
        optipng: {
          optimizationLevel: 7
        },
        mozjpeg: {
          quality: 20
        },
        pngquant: {
          quality: [0.8, 0.9],
          speed: 4
        },
        svgo: {
          plugins: [
            {
              name: 'removeViewBox'
            },
            {
              name: 'removeEmptyAttrs',
              active: false
            }
          ]
        }
      })
    ],
    resolve: {
      alias: [{ find: '@', replacement: resolve(__dirname, './src') }]
    },
    build: {
      target: 'es2015',
      outDir: 'dist',
      rollupOptions: {
        output: {
          manualChunks: {
            echarts: ['echarts']
          }
        }
      }
      // assetsDir: 'assets'
    }
  })
}

```
