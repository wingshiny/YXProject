/*
 * @Author: YinXuan
 * @Date: 2023-01-28 09:13:18
 * @LastEditTime: 2023-01-31 15:12:56
 * @Description: 生成头部注释的快捷键：ctrl + alt + i，生成函数注释的快捷键：ctrl + alt + t
 */
var o = {}; //生成一个新的空对象

//生成一个新的非空对象
o = {
  string: 1, //object 的 key 可以是字符串
  const_var: 2, //object 的 key 也可以是符合变量定义规则的标识符
  func: {}, //object 的 value 可以是任何类型
};

//对象属性的读操作
console.log(1 === o['string']);
console.log(2 === o.const_var);
