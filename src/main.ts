/*
 * @Author: YinXuan
 * @Date: 2023-01-04 09:24:12
 * @LastEditTime: 2023-02-09 14:20:28
 * @Description: 生成头部注释的快捷键：ctrl + alt + i，生成函数注释的快捷键：ctrl + alt + t
 */
import { createApp } from 'vue';
import App from './App.vue';
// router
import router from '@/router';
import 'vant/lib/index.css';
import 'animate.css';

// 创建vue实例
const app = createApp(App);
app.use(router);
// 挂载实例
app.mount('#app');
