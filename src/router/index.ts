import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: '登录',
    },
    component: () => import('@/pages/index.vue'),
  },
  {
    path: '/',
    redirect: '/index',
    meta: { title: '导航' },
    children: [
      {
        path: 'index',
        name: 'Index',
        component: () => import('@/pages/index.vue'),
        meta: { title: '首页' },
      },
      {
        path: 'home',
        name: 'Home',
        component: () => import('@/pages/home/index.vue'),
        meta: { title: '主页' },
      },
      {
        path: '/introduce',
        name: 'INTRODUCE',
        component: () => import('@/pages/introduce/index.vue'),
        meta: { title: '个人介绍' },
      },
      {
        path: '/list',
        name: 'LIST',
        component: () => import('@/pages/list/index.vue'),
        meta: { title: 'demo' },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
export default router;
