/*
 * @Author: YinXuan
 * @Date: 2023-01-04 09:24:12
 * @LastEditTime: 2023-02-23 15:14:57
 * @Description: 生成头部注释的快捷键：ctrl + alt + i，生成函数注释的快捷键：ctrl + alt + t
 */
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import Components from 'unplugin-vue-components/vite';
import { VantResolver } from 'unplugin-vue-components/resolvers';
import * as path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
	resolve: {
		//设置别名
		alias: {
			'@': path.resolve(__dirname, 'src'),
		},
	},
	plugins: [
		vue(),
		Components({
			resolvers: [VantResolver()],
		}),
	],
	build: {
		target: 'es2020',
		minify: 'terser', //压缩方式
		terserOptions: {
			compress: {
				drop_console: true, //剔除console,和debugger
				drop_debugger: true,
			},
		},
		// chunkSizeWarningLimit: 1500,大文件报警阈值设置,不建议使用
		rollupOptions: {
			output: {
				//静态资源分类打包
				chunkFileNames: 'static/js/[name]-[hash].js',
				entryFileNames: 'static/js/[name]-[hash].js',
				assetFileNames: 'static/[ext]/[name]-[hash].[ext]',
				manualChunks(id) {
					//静态资源分拆打包
					if (id.includes('node_modules')) {
						return id.toString().split('node_modules/')[1].split('/')[0].toString();
					}
				},
			},
		},
	},
	optimizedeps: {
		esbuildoptions: {
			target: 'es2020',
		},
	},
	server: {
		port: 8080, //启动端口
		hmr: {
			host: '127.0.0.1',
			port: 8080,
		},
		// 设置 https 代理
		proxy: {
			'/api': {
				target: 'your https address',
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/api/, ''),
			},
		},
	},
});
