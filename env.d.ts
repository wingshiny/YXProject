declare module '*.vue' {
  import type { DefineComponent } from 'vue';
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

/**
 * 在 使用 vue ts 语言开发项目的过程中，会遇到，导入 vue 文件后，提示 找不到模块‘xxx.vue’或其相应的类型声明。
 * （Vue 3 can not find module）
解决方式：
在项目根目录新建一个 xx.d.ts
保存之后，再看刚才编辑器的波浪线报错已经消失了。
 */
